.. image:: https://gitlab.com/lpirl/backup2sftp/badges/master/pipeline.svg
  :target: https://gitlab.com/lpirl/backup2sftp/pipelines
  :align: right

backup2sftp
===========

::

  usage: backup2sftp [-h] [-v] [-d] [--dry-run] [-u USER] [-s SOURCE] [-p PORT]
                     [-b BACKUP] [-c] [-O [SSH_OPTIONS [SSH_OPTIONS ...]]]
                     [-o [RSYNC_OPTIONS [RSYNC_OPTIONS ...]]]
                     host destination
  
  This program helps to push backups of the local system to a remote SFTP/SSH
  site. More specifically, this program is a wrapper around rsync to synchronize
  contents of the local file system hierarchy to a SFTP/SSH server. This program
  contains some convenience logic (e.g., for a remote backup directory) and some
  generic switches for rsync (e.g., work recursively, try to preserve file
  modes).
  
  positional arguments:
    host                  IP address or host name of the SFTP server to connect
                          to
    destination           destination directory; i.e., the directory to back up
                          to
  
  optional arguments:
    -h, --help            show this help message and exit
    -v, --verbose         turn on verbose messages (default: False)
    -d, --debug           turn on debug messages (default: False)
    --dry-run             print but do not run the command which would be
                          executed (default: False)
    -u USER, --user USER  user name for the connection to the SFTP server
                          (default: pygmy)
    -s SOURCE, --source SOURCE
                          source directory; i.e., the directory to back up; /!\️
                          rsync/BSD convention: be well-aware of the trailing
                          slash caveat (default: /)
    -p PORT, --port PORT  port at the SFTP server to connect to (default: 22)
    -b BACKUP, --backup BACKUP
                          backup directory on the remote; i.e., move files to
                          this directory instead of deleting them (default:
                          None)
    -c, --clear-backup    clear the backup directory on the remote before
                          starting the backup (default: False)
    -O [SSH_OPTIONS [SSH_OPTIONS ...]], --ssh-options [SSH_OPTIONS [SSH_OPTIONS ...]]
                          options to pass to SSH; to override default options,
                          provide the contrary option (e.g., '-O=-tX'); specify
                          like '-O=--foo' or '-O=--foo=bar' to avoid
                          interferences with this program's argument parser
                          (default: ['-Tx'])
    -o [RSYNC_OPTIONS [RSYNC_OPTIONS ...]], --rsync-options [RSYNC_OPTIONS [RSYNC_OPTIONS ...]]
                          options to pass to rsync; to override default options,
                          provide the contrary options (e.g., '-o=--no-...',
                          '-o=--include=...'); specify like '-o=--foo' or
                          '-o=--foo=bar' to avoid interferences with this
                          program's argument parser (default: ['--compress', '--
                          human-readable', '--one-file-system', '--fuzzy', '--
                          delete-delay', '--delete-excluded', '--exclude',
                          '/tmp', '--exclude', '**/.CACHE/**', '--exclude',
                          '**/.Cache/**', '--exclude', '**/.cache/**', '--
                          exclude', '**/CACHE/**', '--exclude', '**/Cache/**', '
                          --exclude', '**/cache/**', '--recursive', '--links', '
                          --times', '--perms', '--group', '--owner', '--
                          devices', '--specials', '--xattrs', '--acls', '--fake-
                          super'])
