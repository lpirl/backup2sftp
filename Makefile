SMOKE_TEST_DIR=$(shell pwd)/smoke test

README.rst: README-header.rst backup2sftp
	echo > $@
	cat README-header.rst >> $@
	echo >> $@
	./backup2sftp -h | sed 's/^/  /' >> $@

smoke_test:

	# a directory with a space in its name ;)
	rm -rf "${SMOKE_TEST_DIR}"
	mkdir "${SMOKE_TEST_DIR}"

	# setup artificial source and destination directory
	mkdir -p \
		"${SMOKE_TEST_DIR}/source/cache" \
		"${SMOKE_TEST_DIR}/dest/cache" \
		"${SMOKE_TEST_DIR}/backup"
	touch \
		"${SMOKE_TEST_DIR}/source/source_file" \
		"${SMOKE_TEST_DIR}/source/cache/source_cache_file" \
		"${SMOKE_TEST_DIR}/dest/dest_file" \
		"${SMOKE_TEST_DIR}/dest/cache/dest_cache_file" \
		"${SMOKE_TEST_DIR}/backup/backup_file"

	# run the backup
	./backup2sftp -d \
		-u "$(shell whoami)" \
		-s "${SMOKE_TEST_DIR}/source/" \
		-cb "${SMOKE_TEST_DIR}/backup" \
		localhost \
		"${SMOKE_TEST_DIR}/dest/"

	# assert backup has been cleared:
	[ ! -e  "${SMOKE_TEST_DIR}/backup/backup_file" ]

	# assert source file has been synced:
	[ -e  "${SMOKE_TEST_DIR}/dest/source_file" ]

	# assert pre-existing file has been deleted but also backed up:
	[ ! -e  "${SMOKE_TEST_DIR}/dest/dest_file" ]
	[ -e  "${SMOKE_TEST_DIR}/backup/dest_file" ]

	# assert excluded file are absent:
	[ -e    "${SMOKE_TEST_DIR}/dest/cache" ]
	[ ! -e  "${SMOKE_TEST_DIR}/dest/cache/dest_cache_file" ]
	[ ! -e  "${SMOKE_TEST_DIR}/dest/cache/source_cache_file" ]
